18/8/2017

These REF files were created by TM on his TOSH laptop.

Info about TM's local system:
* Win 10 (64bit).
* IVF compiler 12.1.4.325 (Win32 compiler used).
* SVN revisions:
  - BATEAU_DK r525.
  - DMSL r1726 + extra PATCHes:
    + VARIANT (e310).
    + UTILS (countDelimString d172).
    + bDK_SCRIPT (takeEngineAction d171).
    + DAVID's BC and AR changes (see d173, especially comment:2).
      = DMSL ticket 173: <https://pl1.projectlocker.com/UoNEnvEngGroup/DMSL/trac/ticket/173>).
* DIRECTORs used:
  - FPF: "%DPATH_KORE%/BATEAU_ENF_DEVEL/bateau_setup/bateau_filepaths_ENF.fpf"
  - SKR: "$(BATEAU_ENF_DEVEL)$test/hetero_2017/common/cal_skript_opt_QN10_pp.SKR"
  - PRESKR: "*$(BATEAU_ENF_DEVEL)$test/hetero_2017/masterFiles/bateau_hetero_2017_preskr.txt"
  - MUF:
    + REF_02: "$(BATEAU_ENF_DEVEL)$test/hetero_2017/bateau_muf/B9_GR4J_BCAR1pp_cal.MUF"
    + REF_01: "$(BATEAU_ENF_DEVEL)$test/hetero_2017/bateau_muf/B9_GR4J_BC0.2AR1pp_cal.MUF"
