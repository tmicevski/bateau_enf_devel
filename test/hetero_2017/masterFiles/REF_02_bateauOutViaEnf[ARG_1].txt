BATEAU_ARG_FILE_V1.1
bateauOutViaEnf
[[date='20170818' and time='161621.815']]; infoStr=''
! ---------------------------------------
Arguments [nArg=7]
'(1x,es22.14e3,2x,a,i4,a)' ! format statement
  6.81786176823192E+000  arg[   1] 'x1'
 -2.25428986588249E+000  arg[   2] 'x2'
  3.83048642291070E+000  arg[   3] 'x3'
 -4.95769042525092E-001  arg[   4] 'x4'
 -2.24352935522958E-001  arg[   5] 'lambdaBC'
  1.00000000000000E-005  arg[   6] 'offsetBC'
 -3.71013024576275E-001  arg[   7] 'sdevInno'
! ---------------------------------------
outQval [nOutQ=7]
'(1x,es22.14e3,2x,a,i4,a)' ! format statement
  1.84066398939178E+003  outQval[   1] 'logPost'
  3.71013024576275E-001  outQval[   2] 'logPrior'
  0.00000000000000E+000  outQval[   3] 'logHyperLike'
  1.84029297636721E+003  outQval[   4] 'logLike'
  4.51916923088339E-001  outQval[   5] 'NashSut'
  7.21686511947411E-001  outQval[   6] 'Pearson_R2'
  1.46515798456070E+000  outQval[   7] 'RMS_error'
! ---------------------------------------
OTS_rval0 [nOTS=1]
'(1x,es22.14e3,2x,a,i4,a)' ! format statement
  1.84029297636721E+003  OTS[   1] 'AR1_EM'
! ---------------------------------------
